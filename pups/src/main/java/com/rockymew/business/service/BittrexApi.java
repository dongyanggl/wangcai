package com.rockymew.business.service;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import com.rockymew.core.utility.http.HMACSHA1;
import com.rockymew.core.utility.http.HttpConnectionException;
import com.rockymew.core.utility.http.HttpTool;

import freemarker.log.Logger;

public class BittrexApi {
	

	final String BUY_ORDERBOOK = "buy";
	final String SELL_ORDERBOOK = "sell";
	final String BOTH_ORDERBOOK = "both";
	final String APIKEY="71c15a1d130e466f82bf99bb22266bbd";
	final String APISECRET="6ba60b1262b2451fb5083f59ea4dd918";
	
	final String BASE_URL = "https://bittrex.com/api/v1.1/";
	final String[] MARKET_SET = {"getopenorders", "cancel","sellmarket", "selllimit", "buymarket", "buylimit"};
	
	final String[] ACCOUNT_SET = {"getbalances", "getbalance", "getdepositaddress", "withdraw", "getorderhistory"};
	HttpTool httpTool = new HttpTool();
	public String api_Query(String method, String options){
		 if (options==null||options.length()==0)
	         options = "";
		 LocalDate today = LocalDate.now();
		 String nonce = ""+System.currentTimeMillis();
	     String method_set = "public";
	     
	     for(int i=0;i<MARKET_SET.length;i++)
	     {
	    	 if(MARKET_SET[i].equals(method)){
	    		 method_set = "market";
	    		 break;
	    	 }
	    
	     }
	     
	     for(int i=0;i<ACCOUNT_SET.length;i++)
	     {
	    	 if(ACCOUNT_SET[i].equals(method)){
	    		 method_set = "account";
	    		 break;
	    	 }
	    
	     }
	
	
	
	     String request_url = BASE_URL + method_set+ "/"+ method+ "?";
	
	     if (!method_set.equals("public")) 
	         request_url += "apikey=" + APIKEY + "&nonce=" + nonce + "&";
	
	     request_url += options;
	//
	//     return requests.get(
	//         request_url,
	//         headers={"apisign": hmac.new(self.api_secret.encode(), request_url.encode(), hashlib.sha512).hexdigest()}
	//     ).json()
	     String hmacSHA512 = HMACSHA1.hmacSHA512(APISECRET, request_url);
	     Map header = new HashMap();
	     header.put("apisign", hmacSHA512);
	     try {
			String result = httpTool.get(request_url, header);
			Logger logger = Logger.getLogger("MyLogger");
			logger.debug(result);
		} catch (HttpConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "";
	}
	
	public String get_markets(){
		 return api_Query("getmarkets", null);
	}
	
	public String get_currencies(){
		 return api_Query("getcurrencies", null);
	}
	
	public String get_ticker(String market){
		 return api_Query("getticker", "market="+ market+"&");
	}
	
	public String get_market_summaries(){
		return api_Query("getmarketsummaries", null);
	}
	
	public String get_orderbook(String market, String depth_type, int depth){
		if(depth<=0){
			depth = 20;
		}
		return api_Query("getorderbook", "market="+ market+ "&type="+ depth_type+"&depth="+ depth + "&");
	}
	
	public String get_market_history(String market, int count){
		
		return api_Query("getmarkethistory", "market="+ market +"&count="+ count + "&");
	}
	
	public String buy_market(String market, float quantity){
		
		return api_Query("buymarket", "market="+ market +"&quantity="+ quantity + "&");
	}
	
	public String buy_limit(String market, float quantity, float rate){
		
		return api_Query("buylimit", "quantity="+ market +"&quantity="+ quantity +"&rate="+ rate + "&");
	}
	
	public String sell_market(String market, float quantity){
		
		return api_Query("sellmarket", "quantity="+ market +"&quantity="+ quantity + "&");
	}
	
	public String sell_limit(String market, float quantity, float rate){
		
		return api_Query("selllimit", "quantity="+ market +"&quantity="+ quantity +"&rate="+ rate + "&");
	}

	public String cancel(String uuid){
		 return api_Query("cancel", "uuid="+ uuid+"&");
	}
	
	public String get_open_orders(String market){
		 return api_Query("getopenorders", "market="+ market+"&");
	}
	
	public String get_balances(){
		 return api_Query("getbalances", null);
	}
	
	public String get_balance(String currency){
		 return api_Query("getbalance", "currency="+ currency+"&");
	}
	
	public String get_deposit_address(String currency){
		 return api_Query("getdepositaddress", "currency="+ currency+"&");
	}
	
	public String withdraw(String currency, float quantity, String address){
		 return api_Query("withdraw", "currency="+ currency+"&quantity="+ quantity+ "&address"+ address+"&");
	}
	
	public String get_order_history(String currency, int count){
		 return api_Query("getorderhistory", "currency="+ currency+"&count="+ count+"&");
	}

}
