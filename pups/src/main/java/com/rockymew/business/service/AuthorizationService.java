package com.rockymew.business.service;

import java.util.List;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.rockymew.business.dao.entity.Authorization;
import com.rockymew.business.dao.entity.Sample;
import com.rockymew.business.dao.entity.User;
import com.rockymew.business.dao.mapper.AuthorizationMapper;
import com.rockymew.business.dao.mapper.SampleMapper;
import com.rockymew.business.dao.mapper.UserMapper;
import com.rockymew.core.exception.BusinessCoreException;
import com.rockymew.core.mybatis.paging.Page;
import com.rockymew.core.wsresult.WsResult;


@Service
public class AuthorizationService {
	
	@Resource
	AuthorizationMapper authorizationMapper;

	public void addAuthorization(int userId, String exchangeName, String userKey, String userSecret) {
		Authorization authorization = authorizationMapper.selectByUserKey(userKey);
		if (authorization == null) {
			authorization = new Authorization();
			authorization.setUserId(userId);
			authorization.setExchangeName(exchangeName);;
			authorization.setUserKey(userKey);;
			authorization.setUserSecret(userSecret);;
			authorizationMapper.insert(authorization);
		}

		// List<Sample> data = userMapper.s(page);
		// return WsResult.success("成功了", data);
	}

}
