package com.rockymew.business.service;

import java.util.List;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.rockymew.business.dao.entity.Sample;
import com.rockymew.business.dao.entity.User;
import com.rockymew.business.dao.mapper.SampleMapper;
import com.rockymew.business.dao.mapper.UserMapper;
import com.rockymew.core.exception.BusinessCoreException;
import com.rockymew.core.mybatis.paging.Page;
import com.rockymew.core.wsresult.WsResult;


@Service
public class UserService {
	
	@Resource
	UserMapper userMapper;

	public void addUser(String userName, String userPassword, String userMail, String userPhone) {
		User user = userMapper.selectByUserName(userName);
		if (user == null) {
			user = new User();
			user.setUserName(userName);
			user.setUserPassword(userPassword);
			user.setUserPhone(userPhone);
			user.setUserPassword(userPassword);
			userMapper.insert(user);
		}

		// List<Sample> data = userMapper.s(page);
		// return WsResult.success("成功了", data);
	}

}
