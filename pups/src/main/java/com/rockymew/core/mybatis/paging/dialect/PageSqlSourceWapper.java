package com.rockymew.core.mybatis.paging.dialect;

import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.SqlSource;

import com.rockymew.core.mybatis.paging.PageInfo;

public abstract class PageSqlSourceWapper implements SqlSource{

    protected PageInfo page = null;
    private SqlSource sqlSource;

    public PageSqlSourceWapper( SqlSource sqlSource, PageInfo page ){
        this.sqlSource = sqlSource;
        this.page = page;
    }

    public SqlSource getSqlSrouce(){
        return this.sqlSource;
    }

    @Override
    public BoundSql getBoundSql(Object parameterObject) {
        if( null == this.page ){
            return getCountBoundSql(parameterObject);
        }else{
            return getPageBoundSql(parameterObject);
        }
    }

    public abstract BoundSql getPageBoundSql(Object parameterObject );
    public abstract BoundSql getCountBoundSql(Object parameterObject );
}
