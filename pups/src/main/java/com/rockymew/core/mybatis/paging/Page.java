package com.rockymew.core.mybatis.paging;

public class Page implements PageInfo{
    public static final int DEFAULT_PAGE_SIZE = 10;

    private int pageSize    = DEFAULT_PAGE_SIZE;
    private int pageNumber  = 1;
    private int totalPageCount  = 0;
    private int totalRecordCount = 0;

    public Page() {
    }

    public Page(int page, int size) {
        setPageNumber(page);
        setPageSize(size);
    }

    @Override
    public void setTotalRecordCount(int count) {
        this.totalRecordCount = count;
        this.totalPageCount = (totalRecordCount / this.pageSize) + ((totalRecordCount % this.pageSize == 0) ? 0 : 1);
    }
    
    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
        if( this.pageNumber <= 0 ){
            this.pageNumber = 1;
        }
    }

    @Override
    public int getPageNumber() {
        return this.pageNumber;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
        if( this.pageSize <= 0 ){
            this.pageSize = DEFAULT_PAGE_SIZE;
        }
    }
    
    @Override
    public int getPageSize() {
        return this.pageSize;
    }
    
    public int getTotalPageCount() {
        return totalPageCount;
    }
    
    /**
     * next page
     * @return
     */
    public boolean nextPage(){
        if( this.totalPageCount > this.pageNumber ){
            this.pageNumber++;
            return true;
        }
        return false;
    }
}
