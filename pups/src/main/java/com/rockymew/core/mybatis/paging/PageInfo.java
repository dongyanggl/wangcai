package com.rockymew.core.mybatis.paging;

public interface PageInfo {
    /**
     * set the total count of this select query
     * @param count
     */
    public void setTotalRecordCount(int count);
    
    /**
     * get current page number
     * @return
     */
    public int getPageNumber();
    
    /**
     * get current page size
     * @return
     */
    public int getPageSize();
}
