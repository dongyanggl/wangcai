package com.rockymew.core.mybatis.paging.dialect;

import org.apache.ibatis.mapping.SqlSource;

import com.rockymew.core.mybatis.paging.PageInfo;


public class PageSqlSourceWapperFactory {
    public static PageSqlSourceWapper getPageSqlSource(String dialect, SqlSource sqlSource, PageInfo page ){
        if( "mysql".equals( dialect.toLowerCase() ) ){
            return new MySqlPageSqlSourceWapper( sqlSource, page );
        }
        return null;
    }
}
