package com.rockymew.core.spring.beanfactory.json;

import org.springframework.http.converter.json.Jackson2ObjectMapperFactoryBean;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;

public class MyJackson2ObjectMapperFactoryBean extends Jackson2ObjectMapperFactoryBean{

    private JsonInclude.Include include = null;
    /**
     * Set a custom inclusion strategy for serialization.
     * @see com.fasterxml.jackson.annotation.JsonInclude.Include
     */
    public void setSerializationInclusion(JsonInclude.Include serializationInclusion) {
        include = serializationInclusion;
    }
    
    @Override
    public ObjectMapper getObject() {
        ObjectMapper objectMapper = super.getObject();
        if( null != include ){
            objectMapper.setSerializationInclusion( include );
        }
        return objectMapper;
        
    }
}
