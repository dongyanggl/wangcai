package com.rockymew.core.spring.beanfactory.json;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.springframework.http.converter.json.JacksonObjectMapperFactoryBean;


public class MyJacksonObjectMapperFactoryBean extends JacksonObjectMapperFactoryBean{

    @Override
    public ObjectMapper getObject() {
        ObjectMapper mapper = super.getObject();
        mapper.setSerializationInclusion( Inclusion.NON_NULL );
        
        //mapper.getDeserializationConfig().without(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES);
        return mapper;
    }
}
