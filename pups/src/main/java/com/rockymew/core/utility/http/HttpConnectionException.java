package com.rockymew.core.utility.http;

import java.text.MessageFormat;

public class HttpConnectionException extends Exception{
	private static final long serialVersionUID = -435995765263666212L;
	
	public HttpConnectionException( String url ){
		this( url, null );
	}
	
	public HttpConnectionException( String url, String detail ){
		this.url = url;
		this.detail = detail;
	}
	
	private String url;
	private String detail;
	
	@Override
	public String getMessage() {
		String result = MessageFormat.format("Some bad things happened on connecting ({0}), detail ({1})", url, detail);
		return result;
	}
	
	@Override
	public String toString() {
		return getMessage();
	}
}
