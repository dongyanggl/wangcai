package com.rockymew.core.utility.http;

import java.util.Hashtable;

public class MimeTypeDetector {
    public static Hashtable<String, String> mimeTypeMap = new Hashtable<String, String>(100);
    static{
        mimeTypeMap.put("doc",  "application/msword");
        mimeTypeMap.put("ppt",  "application/vnd.ms-powerpoint");
        mimeTypeMap.put("xls",  "application/vnd.ms-excel");
        
        mimeTypeMap.put("xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        mimeTypeMap.put("docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
        mimeTypeMap.put("pptx", "application/vnd.openxmlformats-officedocument.presentationml.presentation");
        
        mimeTypeMap.put("ez", "application/andrew-inset"); 
        mimeTypeMap.put("hqx", "application/mac-binhex40"); 
        mimeTypeMap.put("cpt", "application/mac-compactpro"); 
        mimeTypeMap.put("bin", "application/octet-stream"); 
        mimeTypeMap.put("dms", "application/octet-stream"); 
        mimeTypeMap.put("lha", "application/octet-stream"); 
        mimeTypeMap.put("lzh", "application/octet-stream"); 
        mimeTypeMap.put("exe", "application/octet-stream"); 
        mimeTypeMap.put("class", "application/octet-stream"); 
        mimeTypeMap.put("so", "application/octet-stream"); 
        mimeTypeMap.put("dll", "application/octet-stream"); 
        mimeTypeMap.put("oda", "application/oda"); 
        mimeTypeMap.put("pdf", "application/pdf"); 
        mimeTypeMap.put("ai", "application/postscript"); 
        mimeTypeMap.put("eps", "application/postscript"); 
        mimeTypeMap.put("ps", "application/postscript"); 
        mimeTypeMap.put("smi", "application/smil"); 
        mimeTypeMap.put("smil", "application/smil"); 
        mimeTypeMap.put("mif", "application/vnd.mif"); 
        mimeTypeMap.put("wbxml", "application/vnd.wap.wbxml"); 
        mimeTypeMap.put("wmlc", "application/vnd.wap.wmlc"); 
        mimeTypeMap.put("wmlsc", "application/vnd.wap.wmlscriptc"); 
        mimeTypeMap.put("bcpio", "application/x-bcpio"); 
        mimeTypeMap.put("vcd", "application/x-cdlink"); 
        mimeTypeMap.put("pgn", "application/x-chess-pgn"); 
        mimeTypeMap.put("cpio", "application/x-cpio"); 
        mimeTypeMap.put("csh", "application/x-csh"); 
        mimeTypeMap.put("dcr", "application/x-director"); 
        mimeTypeMap.put("dir", "application/x-director"); 
        mimeTypeMap.put("dxr", "application/x-director"); 
        mimeTypeMap.put("dvi", "application/x-dvi"); 
        mimeTypeMap.put("spl", "application/x-futuresplash"); 
        mimeTypeMap.put("gtar", "application/x-gtar"); 
        mimeTypeMap.put("hdf", "application/x-hdf"); 
        mimeTypeMap.put("js", "application/x-javascript"); 
        mimeTypeMap.put("skp", "application/x-koan"); 
        mimeTypeMap.put("skd", "application/x-koan"); 
        mimeTypeMap.put("skt", "application/x-koan"); 
        mimeTypeMap.put("skm", "application/x-koan"); 
        mimeTypeMap.put("latex", "application/x-latex"); 
        mimeTypeMap.put("nc", "application/x-netcdf"); 
        mimeTypeMap.put("cdf", "application/x-netcdf"); 
        mimeTypeMap.put("sh", "application/x-sh"); 
        mimeTypeMap.put("shar", "application/x-shar"); 
        mimeTypeMap.put("swf", "application/x-shockwave-flash"); 
        mimeTypeMap.put("sit", "application/x-stuffit"); 
        mimeTypeMap.put("sv4cpio", "application/x-sv4cpio"); 
        mimeTypeMap.put("sv4crc", "application/x-sv4crc"); 
        mimeTypeMap.put("tar", "application/x-tar"); 
        mimeTypeMap.put("tcl", "application/x-tcl"); 
        mimeTypeMap.put("tex", "application/x-tex"); 
        mimeTypeMap.put("texinfo", "application/x-texinfo"); 
        mimeTypeMap.put("texi", "application/x-texinfo"); 
        mimeTypeMap.put("t", "application/x-troff"); 
        mimeTypeMap.put("tr", "application/x-troff"); 
        mimeTypeMap.put("roff", "application/x-troff"); 
        mimeTypeMap.put("man", "application/x-troff-man"); 
        mimeTypeMap.put("me", "application/x-troff-me"); 
        mimeTypeMap.put("ms", "application/x-troff-ms"); 
        mimeTypeMap.put("ustar", "application/x-ustar"); 
        mimeTypeMap.put("src", "application/x-wais-source"); 
        mimeTypeMap.put("xhtml", "application/xhtml+xml"); 
        mimeTypeMap.put("xht", "application/xhtml+xml"); 
        mimeTypeMap.put("zip", "application/zip"); 
        
        mimeTypeMap.put("au", "audio/basic"); 
        mimeTypeMap.put("snd", "audio/basic"); 
        mimeTypeMap.put("mid", "audio/midi"); 
        mimeTypeMap.put("midi", "audio/midi"); 
        mimeTypeMap.put("kar", "audio/midi"); 
        mimeTypeMap.put("mpga", "audio/mpeg"); 
        mimeTypeMap.put("mp2", "audio/mpeg"); 
        mimeTypeMap.put("mp3", "audio/mpeg");
        mimeTypeMap.put("wav", "audio/x-wav");
        mimeTypeMap.put("aif", "audio/x-aiff"); 
        mimeTypeMap.put("aiff", "audio/x-aiff"); 
        mimeTypeMap.put("aifc", "audio/x-aiff"); 
        mimeTypeMap.put("m3u", "audio/x-mpegurl"); 
        mimeTypeMap.put("ram", "audio/x-pn-realaudio"); 
        mimeTypeMap.put("rm", "audio/x-pn-realaudio"); 
        mimeTypeMap.put("rpm", "audio/x-pn-realaudio-plugin"); 
        mimeTypeMap.put("ra", "audio/x-realaudio");
        mimeTypeMap.put("rmvb", "audio/x-pn-realaudio");
        mimeTypeMap.put("rmm", "audio/x-pn-realaudio");

        mimeTypeMap.put("pdb", "chemical/x-pdb"); 
        mimeTypeMap.put("xyz", "chemical/x-xyz");
        
        mimeTypeMap.put("bmp", "image/bmp"); 
        mimeTypeMap.put("gif", "image/gif"); 
        mimeTypeMap.put("ief", "image/ief"); 
        mimeTypeMap.put("jpeg", "image/jpeg"); 
        mimeTypeMap.put("jpg", "image/jpeg"); 
        mimeTypeMap.put("jpe", "image/jpeg"); 
        mimeTypeMap.put("png", "image/png"); 
        mimeTypeMap.put("tiff", "image/tiff"); 
        mimeTypeMap.put("tif", "image/tiff"); 
        mimeTypeMap.put("djvu", "image/vnd.djvu"); 
        mimeTypeMap.put("djv", "image/vnd.djvu"); 
        mimeTypeMap.put("wbmp", "image/vnd.wap.wbmp"); 
        mimeTypeMap.put("ras", "image/x-cmu-raster"); 
        mimeTypeMap.put("pnm", "image/x-portable-anymap"); 
        mimeTypeMap.put("pbm", "image/x-portable-bitmap"); 
        mimeTypeMap.put("pgm", "image/x-portable-graymap"); 
        mimeTypeMap.put("ppm", "image/x-portable-pixmap"); 
        mimeTypeMap.put("rgb", "image/x-rgb"); 
        mimeTypeMap.put("xbm", "image/x-xbitmap"); 
        mimeTypeMap.put("xpm", "image/x-xpixmap"); 
        mimeTypeMap.put("xwd", "image/x-xwindowdump");
        
        mimeTypeMap.put("igs", "model/iges"); 
        mimeTypeMap.put("iges", "model/iges"); 
        mimeTypeMap.put("msh", "model/mesh"); 
        mimeTypeMap.put("mesh", "model/mesh"); 
        mimeTypeMap.put("silo", "model/mesh"); 
        mimeTypeMap.put("wrl", "model/vrml"); 
        mimeTypeMap.put("vrml", "model/vrml"); 
        mimeTypeMap.put("css", "text/css"); 
        mimeTypeMap.put("html", "text/html"); 
        mimeTypeMap.put("htm", "text/html"); 
        mimeTypeMap.put("asc", "text/plain"); 
        mimeTypeMap.put("txt", "text/plain"); 
        mimeTypeMap.put("rtx", "text/richtext"); 
        mimeTypeMap.put("rtf", "text/rtf"); 
        mimeTypeMap.put("sgml", "text/sgml"); 
        mimeTypeMap.put("sgm", "text/sgml"); 
        mimeTypeMap.put("tsv", "text/tab-separated-values"); 
        mimeTypeMap.put("wml", "text/vnd.wap.wml"); 
        mimeTypeMap.put("wmls", "text/vnd.wap.wmlscript"); 
        mimeTypeMap.put("etx", "text/x-setext"); 
        mimeTypeMap.put("xsl", "text/xml"); 
        mimeTypeMap.put("xml", "text/xml"); 
        
        mimeTypeMap.put("mpeg",  "video/mpeg"); 
        mimeTypeMap.put("mpg",   "video/mpeg"); 
        mimeTypeMap.put("mpe",   "video/mpeg"); 
        mimeTypeMap.put("qt",    "video/quicktime"); 
        mimeTypeMap.put("mov",   "video/quicktime"); 
        mimeTypeMap.put("mxu",   "video/vnd.mpegurl"); 
        mimeTypeMap.put("avi",   "video/x-msvideo"); 
        mimeTypeMap.put("movie", "video/x-sgi-movie");
        mimeTypeMap.put("3gp",   "video/3gpp");
        mimeTypeMap.put("mkv",   "video/x-matroska");
        
         
        mimeTypeMap.put("ice", "x-conference/x-cooltalk");
    }
    public static String convert( String ext ){
        if( null == ext || ext.isEmpty() ){
            return "application/octet-stream";
        }
        String mimeType = mimeTypeMap.get(ext.toLowerCase());
        if( null == mimeType ){
            return "application/octet-stream";
        }
        return mimeType;
    }
}
