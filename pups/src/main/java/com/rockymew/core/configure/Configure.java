package com.rockymew.core.configure;

import java.util.Properties;

public class Configure {
    public static String getProperty( String key ){
        return properties.getProperty(key);
    }

    public static String getProperty( String key, String defaultValue ){
        return properties.getProperty(key, defaultValue);
    }

    private static Properties properties = null;

    public static Properties getProperties() {
        return properties;
    }

    public static void setProperties(Properties properties) {
        Configure.properties = properties;
    }
}
