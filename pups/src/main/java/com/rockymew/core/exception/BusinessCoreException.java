package com.rockymew.core.exception;

import com.rockymew.core.wsresult.WsResult;

public class BusinessCoreException extends RuntimeException{
    private static final long serialVersionUID = 1L;
    public BusinessCoreException(){
        
    }
    
    public BusinessCoreException(String msg){
        super(msg);
        this.msg = msg;
    }
    
    public BusinessCoreException(String msg, Object data){
        super(msg);
        this.msg = msg;
        this.data = data;
    }
    
    public BusinessCoreException(Integer result, String msg){
        super(msg);
        this.result = result;
        this.msg = msg;
    }
    
    public BusinessCoreException(Integer result, String msg, Object data){
        super(msg);
        this.result = result;
        this.msg = msg;
        this.data = data;
    }
    
    @Override
    public String toString() {
        return msg;
    }
    
    private Integer result = 1;
    private String  msg = "";
    private Object  data = null;
    
    public WsResult toWsResult(){
        return new WsResult(result, msg, data );
    }
}
