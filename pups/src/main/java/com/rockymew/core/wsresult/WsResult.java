package com.rockymew.core.wsresult;

public class WsResult{
    public static Integer RESULT_SUCCESS = 0;
    public static Integer RESULT_GENERAL_FAILED = 1;
    
    public WsResult( Integer result, String msg, Object data ){
        this.result = result;
        this.msg = msg;
        this.data = data;
    }
    
    public static WsResult success( String msg ){
        return new WsResult(0, msg, null);
    }
    
    public static WsResult success( String msg, Object data ){
        return new WsResult(0, msg, data);
    }
    
    public static WsResult failed( String msg ){
        return new WsResult(1, msg, null);
    }
    
    public static WsResult failed( String msg, Object data ){
        return new WsResult(1, msg, data);
    }
    
    private Object  data;
    private Integer result;
    private String  msg;
    
    public Object getData() {
        return data;
    }
    public void setData(Object data) {
        this.data = data;
    }
    public Integer getResult() {
        return result;
    }
    public void setResult(Integer result) {
        this.result = result;
    }
    public String getMsg() {
        return msg;
    }
    public void setMsg(String msg) {
        this.msg = msg;
    }
}
