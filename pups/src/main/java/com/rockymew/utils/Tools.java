package com.rockymew.utils;

import javax.servlet.http.HttpServletRequest;

public final class Tools {

    public static final String currentPath(HttpServletRequest request) {
        String path = request.getSession().getServletContext().getRealPath("/");
        path = path.replaceAll("\\\\", "/");
        return path.endsWith("/") ? path : (path + "/");
    }

}
