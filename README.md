
# wangcai
区块链项目旺财源码


1. web - 网页代码
2. solidity - 合约代码
3. petServer - Java 后台
4. blockSyncServer - 服务器同步数据代码
5. pups - 根据小狗图片生成代码


ethereum 1.8.3


##1. 编译合约：

以下内容基于 truffle v4.1.6

cd solidity（newPet） 目录

1. truffle compile
	
	编译合约，在 build 中生成对应的文件

2. npm run flat

	生成 flat 合约，在 flat_contracts 中生成对应的文件
	
	其中：

	GenePupFlat.sol 基因合约
	
	Pupcoreflat.sol 核心合约
	
	SaleClockAuctionFlat.sol 出售合约
	
	SiringClockAuctionFlat.sol 繁殖合约

3. truffle develop 

	运行 ETH 测试链，注意端口号，其他程序需要做相应的配合，默认为 9545
	
	执行 truffle develop 后，可以直接在命令中输入web3.js 的常用命令查看数据
	
##2. 数据库

1. 使用 MySQL 创建 pet 数据库，不需要创建任何表
2. 当执运行 JAVA 后台程序后（3.5），会自动生成需要的表
3. 在表中增加以下内容
	
	1. genes 中增加一条默认的基因
		
		|字段|内容|
		|---|---|
		|id|1| 
		|genes|00001084210842108421144008421294a5ce31cce31c09081080ba108427bdef| 
		|state|0|
		|type|1|
	
	2. last\_sync\_block 始终至少保留一条内容

		|字段|内容|
		|---|---|
		|id|1| 
		|block_number|0| 
		
4. 当程序正常运行时

	pet 中会增加小狗的数据		
	last\_sync\_block 会不断同步区块链的最新块的内容
	
##3. 部署合约（JAVA）

部署合约通过 JAVA 程序部署，所以需要搭建 JAVA 环境。

1. 在 Eclipse 中导入项目 blockSyncServer

	这里可能会遇到一些问题，部分问题会记录在后面的 QA 中

2. 修改 application.properties 中的数据库以及ETH测试链(web3j)

		# Jdbc datasource 
		spring.datasource.url = jdbc:mysql://localhost:3306/pet?characterEncoding=utf8&useSSL=false
		spring.datasource.username = root
		spring.datasource.password = 123456
		spring.datasource.driverClassName = com.mysql.jdbc.Driver
	
		# web3j 
		web3j.client-address=http://localhost:9545
		web3j.admin-client=false
		web3j.network-id=*

3. ./mvnw web3j:generate-sources 将合约代码生成java代码

	此命令要在 blockSyncServer 目录下执行，即 mvnw 同级目录
	
	注意需要将 pom.xml 中的 web3j-maven-plugin 指定 flat_contracts 目录为编译合约中最后生成的 flat_contracts 路径


生成java代码在 src/mian/java/generated.com.dapp.....

4. 将生成的合约 java 代码拷贝到 /Users/wdy/dev/src/others/wangcai/petServerBlock/src/main/java/com/dapp/pet/core/model 中 

5. 右键 com.dapp.pet/Application.java Run Application

	正常启动后会有错误，是因为没有发布尝试执行合约导致，先不用管。
	
	先看数据库中是否自动生成了需要的表格。
	
	如果没有，检查 application.properties 中 MYSQL 的配置
	
6. 浏览器访问 localhost:8080/block/deploy

	接口代码在：com/dapp/pet/block/BlockController.java

	返回格式为：
		
		["0x8cdaf0cd259887258bc13a92c0a6da92698644c0","0xf12b5dd4ead5f743c6baa640b0216200e89b60da","0x345ca3e014aaf5dca488057592ee47305d9b3e10","0xf25186b5081ff5ce73482ad761db0eb0d25abfbf"]
		
	把这些内容按顺序更新到 application.properties 中的合约地址属性中，按顺序分别为核心合约地址，销售合约地址，繁殖合约地址以及基因合约地址。现在程序应该只需要前三个，不需要最后一个。
	
		# core contract address 
		app.petContractAddress=0x8cdaf0cd259887258bc13a92c0a6da92698644c0
		app.petSaleContractAddress=0xf12b5dd4ead5f743c6baa640b0216200e89b60da
		app.petSiringContractAddress=0x345ca3e014aaf5dca488057592ee47305d9b3e10
		app.petGeneContractAddress=0xf25186b5081ff5ce73482ad761db0eb0d25abfbf


##4. 启动同步（JAVA）

合约部署完成并在配置文件中配置好以后，需要执行启动，后台才会同步区块链的内容。

修改0代宠物生成时间

com/dapp/pet/block/BlockService.java 中 createGen0Auction 间隔减少到 1 分钟，方便进行测试

1. 浏览器访问接口 http://localhost:8080/block/start?gen0Started=true&txSyncStarted=true&giveBirthStarted=true&promStarted=true

	正常没有什么返回数据，但1分钟以后，数据库中的 pet 表中会生成一条0代猫的数据

	也可以根据 Log 来判断是否成功，比如 birthEvent 则是宠物的数据

2. 浏览器访问 http://localhost:8080/block/status

	用于查询当前服务器的一些状态值
	
	
	
## QA


1. Java 程序编译出错

	1. MAC 下 Getter/Setter 不支持

	*原因：* lombok 插件没有安装成功。
	
	解决方案：
	
	1. 官网下载lombok.jar和最新的 Eclipse（201804 我用的这个版本，老版本好像不好用）
	
	2. 找到Eclipse应用（在Mac上就是Eclipse.app文件），选择打开包内容，找到eclipse.ini
	
	3. 将lombok.jar拷贝至eclipse.ini所在目录下，并在eclipse.ini文件最后加上如下两行
	
		-Xbootclasspath/a:lombok.jar
		-javaagent:/Users/abc/Eclipse/eclipse-jee-mars-1-macosx-cocoa-x86_64/Eclipse.app/Contents/Eclipse/lombok.jar
	
	 原则上执行 java -jar lombok.jar，然后选择对应的 Eclipse 也应该一样，但我这不成功，最后还是手动修改
	 
	4. 重启eclipse

2. 执行 truffle compile 失败

	1. 提示无法解析 ClockAuctionBase.sol

		*原因：*该文件内容有错误
		
		解决方案：删除最后面的数字即可

	2. truffle 版本可能有问题，通过测试的版本为 4.1.6

	
3. /mvnw web3j:generate-sources 执行出错

	1. 怀疑是 solidity 没有按照成功，按照下面方法安装 solidity(solc) 后，可以了
	


4. 安装 solidity

	1. MAC 上执行 brew install solidity 最后提示 


			Error: You have MacPorts or Fink installed:
			  /sw/bin/fink

		解决方案：根据提示尝试将 fink 临时移动到其他目录

		错误提示：
		
			make install
			Last 15 lines from /Users/wdy/Library/Logs/Homebrew/solidity/02.make:
			Undefined symbols for architecture x86_64:

			...
			
			
		解决方案：因为 solidity v4.2.2 编译环境有问题，本机安装了 jsoncpp v1.8.4，导致出现了识别的问题。卸载 jsoncpp 后，重新运行 brew install solidity 后，可用
		
		卸载可能有依赖，强制卸载 “brew uninstall --ignore-dependencies jsoncpp”
		
		生成的 solc 在 /usr/local/Cellar/solidity/0.4.22/bin 中，把这里的 solc 加入到系统环境中
		
5. JAVA 后台启动没有在数据库中生成小狗

	可能是以下一些原因：
	
	1. 测试链重新启动过，没有部署合约或者部署的合约有问题
	2. JAVA 后台启动后没有执行 start

			localhost:8080/block/start?gen0Started=true&txSyncStarted=true&giveBirthStarted=true&promStarted=true

	3. 数据库里的数据有问题
	
		1. genes 中增加一条默认的基因
			
			|字段|内容|
			|---|---|
			|id|1| 
			|genes|00001084210842108421144008421294a5ce31cce31c09081080ba108427bdef| 
			|state|0|
			|type|1|
			
		2. last\_sync\_block 始终至少保留一条内容
	
			|字段|内容|
			|---|---|
			|id|1| 
			|block_number|0| 
	
		3. genes 中的数据被都生成了小狗

			可以将 pet 中的数据清空，同时将 genes 中的 state 字段都设置成0
		

		
## Linux 测试链环境搭建

1. 安装 Node.js 和 npm

	-- yum install nodejs 这个方法安装的版本太老
	
	https://nodejs.org/dist/ 下载最新的版本，解压，bin目录设置成系统目录
	
	node v9.9.0
	npm v5.6.0

	
2. 安装 truffle 

	npm install -g truffle

	truffle@4.1.6

3. 安装 solidity(solc) 和 solc-cli

		npm install -g solc
		solc@0.4.22
		
		npm install -g solc-cli

4. 编译合约

	清空 build 和 flat_contracts 目录
	
		truffle compile
		npm run flat

	如不需要这个环境编译合约，这步可以省略，

	可能出现 truffle-flattener 没有权限，增加可执行权限即可
	提示找不到 ../truffle-flattener/index.js，可能 truffle-flattener 插件没有安装正确

5. 运行测试环境

		truffle develop

6. 保持后台运行

	测试发现使用 nohub & 不能保持后台运行
	
	采用 screen 方法
	
	1. 安装 screen 

			yum install screen
	
	2. 启动 screen （可以不输入名字）

			screen -S blockpet
			
	3. 在启动后的 screen 里运行 truffle develop
	4. 关闭终端，程序依然能够访问
	5. 重新加入 screen
	
		1. 重新登录终端，通过 -ls 参数查看存在的 screen
		
			screen -ls 
			
		返回内容
					
			There are screens on:
				32554.blockpet	(Detached)
				32664.blockpet2	(Detached)
		2. screen -r 32554
		
	6. 退出 screen

		停止 screen 中的运行的程序，最后执行 exit

		提示：		
		
			[screen is terminating]

